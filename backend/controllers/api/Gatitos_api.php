<?php
require APPPATH.'/libraries/REST_Controller.php';
 
class gatitos_api extends REST_Controller{
    function __construct(){
             parent::__construct();         
             $this->load->model('gatitos_model');     
         }

public function gatitos_get()
{
    if(! $this->get('id'))
    {
        $gatitos = $this->gatitos_model->get_todo(); // return all record
    } else {
        $gatitos = $this->gatitos_model->get_gatito($this->get('id')); // return a record based on id
    }
 
    if($gatitos)
    {
        $this->response($gatitos, 200); // return success code if record exist
    } else {
        $this->response([], 404); // return empty
    }
}

public function gatito_post()
{
    if(! $this->post('nombre') && ! $this->post('edad') && ! $this->post('peso'))
    {
        $this->response(array('error' => 'Se necesita nombre,peso y edad'), 400);
    }
    else{
        $data = array(
            'nombre' => $this->post('nombre'),
            'edad' => $this->post('edad'),
            'peso' => $this->post('peso')
        );
    }
    $this->db->insert('gatitos',$data);
    if($this->db->insert_id() > 0)
    {
        $message = array('id' => $this->db->insert_id(), 'nombre' => $this->post('nombre'));
        $this->response($message, 200);
    }
}

public function gatito_put()
{
    if(! $this->put('id'))
    {
        $this->response(array('error' => 'id necesario'), 400);
    }
 
    $data = array(
        'id' => $this->put('id'),
        'nombre'     => $this->put('nombre'),
        'edad'    => $this->put('edad'),
        'peso'    => $this->put('peso')
    );
    $this->gatitos_model->update_gatito($data);
    $message = array('success' => $this->put('title').' Updated!');
    $this->response($message, 200);
}

public function gatito_delete($id=NULL)
{
    if($id == NULL)
    {
        $message = array('error' => 'Se necesita un id');
        $this->response($message, 400);
    } else {
        $this->gatitos_model->delete_gatito($id);
        $message = array('id' => $id, 'message' => 'Eliminado');
        $this->response($message, 200);
    }
}

}