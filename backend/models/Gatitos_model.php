<?php  
class Gatitos_model extends CI_Model{     
	function __construct(){         
		parent::__construct();         
		$this->load->database();     
	}       

	function get_todo(){
	        $sql= "select * from gatitos";
	        return $this->db->query($sql)-> result_array(); 		
	    }

	function get_gatito($id){
	        $sql= "select * from gatitos where id=?";
	        return $this->db->query($sql,array("$id"));		
	    }

	 function update_gatito($data){
	        $sql= "update gatitos set nombre=?,edad=?,peso=? where id=?";
	        return $this->db->query($sql,array('$data["nombre"]','$data["edad"]','$data["peso"]','$data["id"]'));
	    }

	 function delete_gatito($id){
	        $sql= "delete from gatitos where id=?";
	        return $this->db->query($sql,array("$id"));
	    }    
	}