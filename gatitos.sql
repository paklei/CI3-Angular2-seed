CREATE TABLE IF NOT EXISTS `gatitos` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(15) NOT NULL,
  `peso` tinyint NOT NULL,
  `edad` tinyint NOT NULL,
  `f_registro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;