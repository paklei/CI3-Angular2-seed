import {Component,Directive, OnInit, Type} from '@angular/core';
import {Http} from '@angular/http';
import {FormGroup, FormControl, Validators, FormBuilder} from '@angular/forms';
import * as directives from '@angular/router';

import {GatitosService} from './gatitos/gatitos.service';

@Component({
	selector: 'app',
	templateUrl: '/frontend/app.component.html',
})

export class AppComponent implements OnInit {

	private gatitos = [];
	private isLoading = true;

	private gatito = {};
	private isEditing = false;

	private addGatitoForm: FormGroup;
	private nombre = new FormControl("", Validators.required);
	private edad = new FormControl("", Validators.required);
	private peso = new FormControl("", Validators.required);

	private infoMsg = { body: "", type: "info"};

	constructor(private http: Http,
				private gatitosService: GatitosService,
				private formBuilder: FormBuilder) {	}

	ngOnInit() {
		this.getGatitos();

		this.addGatitoForm = this.formBuilder.group({
			nombre: this.nombre,
			edad: this.edad,
			peso: this.peso
		});
	}

	getGatitos() {
		this.gatitosService.getGatitos().subscribe(
			data => this.gatitos = JSON.parse(data['_body']),
			error => console.log(error),
			() => this.isLoading = false
		);
	}

	addGatito() {
		this.gatitosService.addGatito(this.addGatitoForm.value).subscribe(
			res => {
				var newGatito = res.json();
				this.gatitos.push(newGatito);
				this.addGatitoForm.reset();
				this.sendInfoMsg("Registro correcto.", "Ok");
			},
			error => console.log(error)
		);
	}

	enableEditing(gatito) {
		this.isEditing = true;
		this.gatito = gatito;
	}

	cancelEditing() {
		this.isEditing = false;
		this.gatito = {};
		this.sendInfoMsg("Edición cancelada.", "warning");
		// reload the cats to reset the editing
		this.getGatitos();
	}

	editGatito(gatito) {
		this.gatitosService.editGatito(gatito).subscribe(
			res => {
				this.isEditing = false;
				this.gatito = gatito;
				this.sendInfoMsg("Edición correcta.", "Ok");
			},
			error => console.log(error)
		);
	}

	deleteGatito(gatito) {
		if(window.confirm("Realizar borrado?")) {
			this.gatitosService.deleteGatito(gatito).subscribe(
				res => {
					//var pos = this.gatito.map(gatito => { return gatito.id }).indexOf(gatito.id);
					//this.gatitos.splice(pos, 1);
					this.sendInfoMsg("Registro borrado.", "Ok");
				},
				error => console.log(error)
			);
		}
	}

	sendInfoMsg(body, type, time = 2000) {
		this.infoMsg.body = body;
		this.infoMsg.type = type;
		window.setTimeout(() => this.infoMsg.body = "", time);
	}

}
