import {Injectable} from '@angular/core';
import {Http, Headers, RequestOptions} from '@angular/http';

@Injectable()

export class GatitosService {

    private headers = new Headers({ 'Content-Type': 'application/json', 'charset': 'UTF-8' });
    private options = new RequestOptions({ headers: this.headers });

    constructor (private http: Http) {}

    getGatitos() {
        //console.log(this.http.get('/api/gatitos_api/gatitos'));
         return this.http.get('/api/gatitos_api/gatitos');

    }

    addGatito(gatito) {
        return this.http.post("/api/gatitos_api/gatito", JSON.stringify(gatito), this.options);
    }

    editGatito(gatito) {
        return this.http.put("/api/gatitos_api/gatito/"+gatito.id, JSON.stringify(gatito), this.options);
    }

    deleteGatito(gatito) {
        return this.http.delete("/api/gatitos_api/gatito/"+gatito.id, this.options);
    }

}
