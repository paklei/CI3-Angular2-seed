"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var platform_browser_1 = require('@angular/platform-browser');
var router_1 = require('@angular/router');
var http_1 = require('@angular/http');
var forms_1 = require('@angular/forms');
var gatitos_component_1 = require('./gatitos.component');
var gatitos_service_1 = require('./gatitos.service');
var core_2 = require('@angular/core');
core_2.enableProdMode();
var routing = router_1.RouterModule.forRoot([
    { path: '/gatitosComponent', component: gatitos_component_1.GatitosComponent },
]);
var GatitosModule = (function () {
    function GatitosModule() {
    }
    GatitosModule = __decorate([
        core_1.NgModule({
            imports: [platform_browser_1.BrowserModule,
                routing,
                http_1.HttpModule,
                forms_1.FormsModule,
                forms_1.ReactiveFormsModule],
            declarations: [gatitos_component_1.GatitosComponent],
            providers: [gatitos_service_1.GatitosService]
        }), 
        __metadata('design:paramtypes', [])
    ], GatitosModule);
    return GatitosModule;
}());
exports.GatitosModule = GatitosModule;
//# sourceMappingURL=gatitos.module.js.map