"use strict";
var router_1 = require('@angular/router');
var app_component_1 = require('./app.component');
exports.routes = [
    { path: '', component: app_component_1.AppComponent }
];
exports.routing = router_1.RouterModule.forRoot(exports.routes, { useHash: true });
//# sourceMappingURL=routes.js.map