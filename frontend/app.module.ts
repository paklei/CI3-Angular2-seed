import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {HttpModule} from '@angular/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { NgSemanticModule } from "ng-semantic";

import {AppComponent} from './app.component';
import { routing } from "./routes";
import {GatitosService} from './gatitos/gatitos.service';


import {enableProdMode} from '@angular/core';
enableProdMode();

@NgModule({
    imports: [BrowserModule,
    		  routing,
    		  HttpModule,
    		  FormsModule,
    		  ReactiveFormsModule
              ],
    declarations: [AppComponent],
    providers: [GatitosService],
    bootstrap: [AppComponent]
})

export class AppModule {}
