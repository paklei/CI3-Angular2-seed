"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
var forms_1 = require('@angular/forms');
var gatitos_service_1 = require('./gatitos/gatitos.service');
var AppComponent = (function () {
    function AppComponent(http, gatitosService, formBuilder) {
        this.http = http;
        this.gatitosService = gatitosService;
        this.formBuilder = formBuilder;
        this.gatitos = [];
        this.isLoading = true;
        this.gatito = {};
        this.isEditing = false;
        this.nombre = new forms_1.FormControl("", forms_1.Validators.required);
        this.edad = new forms_1.FormControl("", forms_1.Validators.required);
        this.peso = new forms_1.FormControl("", forms_1.Validators.required);
        this.infoMsg = { body: "", type: "info" };
    }
    AppComponent.prototype.ngOnInit = function () {
        this.getGatitos();
        this.addGatitoForm = this.formBuilder.group({
            nombre: this.nombre,
            edad: this.edad,
            peso: this.peso
        });
    };
    AppComponent.prototype.getGatitos = function () {
        var _this = this;
        this.gatitosService.getGatitos().subscribe(function (data) { return _this.gatitos = JSON.parse(data['_body']); }, function (error) { return console.log(error); }, function () { return _this.isLoading = false; });
    };
    AppComponent.prototype.addGatito = function () {
        var _this = this;
        this.gatitosService.addGatito(this.addGatitoForm.value).subscribe(function (res) {
            var newGatito = res.json();
            _this.gatitos.push(newGatito);
            _this.addGatitoForm.reset();
            _this.sendInfoMsg("Registro correcto.", "Ok");
        }, function (error) { return console.log(error); });
    };
    AppComponent.prototype.enableEditing = function (gatito) {
        this.isEditing = true;
        this.gatito = gatito;
    };
    AppComponent.prototype.cancelEditing = function () {
        this.isEditing = false;
        this.gatito = {};
        this.sendInfoMsg("Edición cancelada.", "warning");
        // reload the cats to reset the editing
        this.getGatitos();
    };
    AppComponent.prototype.editGatito = function (gatito) {
        var _this = this;
        this.gatitosService.editGatito(gatito).subscribe(function (res) {
            _this.isEditing = false;
            _this.gatito = gatito;
            _this.sendInfoMsg("Edición correcta.", "Ok");
        }, function (error) { return console.log(error); });
    };
    AppComponent.prototype.deleteGatito = function (gatito) {
        var _this = this;
        if (window.confirm("Realizar borrado?")) {
            this.gatitosService.deleteGatito(gatito).subscribe(function (res) {
                //var pos = this.gatito.map(gatito => { return gatito.id }).indexOf(gatito.id);
                //this.gatitos.splice(pos, 1);
                _this.sendInfoMsg("Registro borrado.", "Ok");
            }, function (error) { return console.log(error); });
        }
    };
    AppComponent.prototype.sendInfoMsg = function (body, type, time) {
        var _this = this;
        if (time === void 0) { time = 2000; }
        this.infoMsg.body = body;
        this.infoMsg.type = type;
        window.setTimeout(function () { return _this.infoMsg.body = ""; }, time);
    };
    AppComponent = __decorate([
        core_1.Component({
            selector: 'app',
            templateUrl: '/frontend/app.component.html',
        }), 
        __metadata('design:paramtypes', [http_1.Http, gatitos_service_1.GatitosService, forms_1.FormBuilder])
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;
//# sourceMappingURL=app.component.js.map